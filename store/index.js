export const state = () => ({
    register:{
        Import: 0,
        Export: 0,
        all: 100,
        Balance: 0,
        Date:''
    }
})
export const getters = {
    getRegister(state){
        return state.register
    },
}
export const mutations = {
    SET_REGISTER(state, data){
        state.register = {
            ...state.register,
            ...data
        }
    },
}
export const actions = {
    setRegister({ commit }, data){
        commit('SET_REGISTER', data)
    },
}